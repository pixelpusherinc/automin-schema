# AutoMin Schema

This module is used by automin-client and automin-server.

These are the core schema types and classes. This codebase is client/server agnostic.

## Build from source

	npm run build

## Build Docs

	npm run build-docs

### Serve Docs

	npm run serve-docs

Then go to http://localhost:3000 in your browser.

### Clean Docs (delete generated docs directory)

	num run clean-docs
