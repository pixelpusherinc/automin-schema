"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function capitalizeFirst(s) {
    if (!s)
        return s;
    return s.charAt(0).toUpperCase() + s.slice(1);
}
exports.capitalizeFirst = capitalizeFirst;
