"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function create(i) {
    if (i === void 0) { i = {}; }
    var ui = {
        home: i.home || { link: { text: "Home", url: "./" } },
        topNav: i.topNav || { links: [] },
        sideNav: i.sideNav || { sections: [] },
        welcome: i.welcome || { title: '', bodyText: '', bodyHTML: undefined }
    };
    return ui;
}
exports.create = create;
