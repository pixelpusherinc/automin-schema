"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var schemanode_1 = require("../schemanode");
var field_1 = require("../field");
var dict_1 = require("../lib/dict");
var Entity = /** @class */ (function (_super) {
    __extends(Entity, _super);
    function Entity(i, p) {
        var _this = _super.call(this, i, p) || this;
        _this.labelPlural = i.labelPlural ?
            i.labelPlural : Entity.pluralize(_this.label);
        // Fields...
        _this.orderField = undefined;
        var pkeys = 0;
        _this.fields = i.fields.map(function (f) {
            if (f.primaryKey) {
                if (pkeys > 1) {
                    throw new Error("Entity '" + _this.name + "' cannot have more than one primary key.");
                }
                pkeys++;
            }
            if (f.type === 'order') {
                if (_this.orderField) {
                    throw new Error("Entity '" + _this.name + " cannot have more than one order field.");
                }
            }
            var field = field_1.Field.create(f, _this);
            if (field.type === 'order') {
                _this.orderField = field;
            }
            return field;
        });
        if (i.listViewFields) {
            // Schema has specified fields to appear in list view
            _this.listViewFields = i.listViewFields.map(function (f) {
                if (!_this.getField(f)) {
                    throw new Error("Field '" + f + "' in listViewFields for entity '" + _this.name + "' does not exist.");
                }
                return f;
            });
        }
        else {
            // Use all fields in list view
            _this.listViewFields = i.fields.map(function (f) { return f.name; });
        }
        _this.pageRowCount = i.pageRowCount || 25;
        if (i.defaultSort) {
            _this.defaultSort = i.defaultSort;
        }
        else {
            if (_this.orderField) {
                _this.defaultSort = [{ field: _this.orderField.name, order: 'ASC' }];
            }
            else {
                _this.defaultSort = [{ field: _this.getPrimaryKey().name, order: 'ASC' }];
            }
        }
        return _this;
    }
    /**
     * Find a field in this entity by name.
     */
    Entity.prototype.getField = function (fname) {
        return this.fields.find(function (f) { return f.name === fname; });
    };
    /**
     * Returns the primary key field.
     */
    Entity.prototype.getPrimaryKey = function () {
        return this.fields.find(function (f) { return !!f.primaryKey; });
    };
    /**
     * Returns array of fields to show in list view.
     */
    Entity.prototype.getListFields = function () {
        var _this = this;
        var fields;
        if (this.listViewFields && this.listViewFields.length > 0) {
            fields = this.listViewFields.map(function (fname) { return _this.getField(fname); });
        }
        else {
            fields = this.fields;
        }
        return fields;
    };
    /**
     * Determine if this entity has any updateable fields
     */
    Entity.prototype.updateable = function () {
        return !!this.fields.find(function (f) { return f.updateable(); });
    };
    Entity.prototype.resolveReferences = function () {
        this.fields.forEach(function (f) {
            f.resolveReferences();
        });
    };
    /**
     * Attempts to validate a record (for update/create)
     */
    Entity.prototype.validateData = function (data, mode) {
        if (mode !== 'create' && mode !== 'edit') {
            throw new Error("Incorrect mode for validateData: " + mode);
        }
        var errors = dict_1.default();
        this.fields.forEach(function (field) {
            var err = field.validateData(data, mode);
            if (err) {
                errors[field.name] = err;
            }
        });
        return Object.keys(errors).length > 0 ? errors : undefined;
    };
    Entity.create = function (i, p) {
        return new Entity(i, p);
    };
    Entity.pluralize = function (s) {
        var p = s;
        s = s.toLowerCase();
        if (s.endsWith('s')) {
            // nothing to do
        }
        else if (s.endsWith('y') && !s.endsWith('ey')) {
            p = p.substr(0, p.length - 1) + 'ies';
        }
        else {
            p += 's';
        }
        return p;
    };
    return Entity;
}(schemanode_1.SchemaNode));
exports.Entity = Entity;
