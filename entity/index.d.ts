import { ISchemaNode, SchemaNode } from '../schemanode';
import { IField, Field, Mode } from '../field';
import { OrderField } from '../field/order';
import D from '../lib/dict';
export interface Filter {
    fieldName: string;
    text: string;
}
export interface Sort {
    field: string;
    order: 'ASC' | 'DESC';
}
/**
 * Entity represents a DB table.
 */
export interface IEntity extends ISchemaNode {
    /** If provided, will be used rather than attempting to automatically pluralize label. */
    labelPlural?: string;
    /** List of fields in this entity */
    fields: IField[];
    /** Optional list of fields to display in list views. Defaults to displaying all. */
    listViewFields?: string[];
    /** Number of rows displayed per page. */
    pageRowCount?: number;
    /** Default list sort. Array of {field: string, order: 'ASC' | 'DESC'} */
    defaultSort?: Sort[];
}
export declare class Entity extends SchemaNode implements IEntity {
    labelPlural: string;
    fields: Field[];
    listViewFields: string[];
    pageRowCount: number;
    defaultSort: Sort[];
    orderField?: OrderField;
    constructor(i: IEntity, p?: SchemaNode);
    /**
     * Find a field in this entity by name.
     */
    getField(fname: string): Field | undefined;
    /**
     * Returns the primary key field.
     */
    getPrimaryKey(): Field;
    /**
     * Returns array of fields to show in list view.
     */
    getListFields(): Field[];
    /**
     * Determine if this entity has any updateable fields
     */
    updateable(): boolean;
    resolveReferences(): void;
    /**
     * Attempts to validate a record (for update/create)
     */
    validateData(data: D<any>, mode: Mode): D<string> | void;
    static create(i: IEntity, p?: SchemaNode): Entity;
    static pluralize(s: string): string;
}
