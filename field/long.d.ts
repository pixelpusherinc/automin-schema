import { SchemaNode } from '../schemanode';
import { IIntField, IntField } from './int';
export interface ILongField extends IIntField {
}
export declare class LongField extends IntField implements ILongField {
    constructor(f: IIntField, p?: SchemaNode);
}
