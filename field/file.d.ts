import { SchemaNode } from '../schemanode';
import { IField, Field } from '../field';
export interface IFileField extends IField {
    /** URL to upload to */
    uploadUrl: string;
    /** Name to send along with upload. Defaults to fully-qualified field name. */
    uploadName?: string;
    /** URL path to permanent storage directory (excluding filename) */
    filePath?: string;
    /** URL path to temporary storage directory (excluding filename) */
    tempPath?: string;
    /** Allowed extensions. (Eg: ['png','jpg']). Defaults to any. */
    extensions?: string[];
}
export declare class FileField extends Field implements IFileField {
    uploadUrl: string;
    uploadName?: string;
    filePath?: string;
    tempPath?: string;
    extensions?: string[];
    constructor(f: IFileField, p?: SchemaNode);
}
