import { SchemaNode } from '../schemanode';
import { IField, Field, Mode } from '../field';
import D from '../lib/dict';
export interface IBooleanField extends IField {
    defaultValue?: boolean;
}
export declare class BooleanField extends Field implements IBooleanField {
    defaultValue?: boolean;
    constructor(f: IBooleanField, p?: SchemaNode);
    validateData(data: D<any>, mode: Mode): string | void;
}
