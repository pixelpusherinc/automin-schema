import { SchemaNode } from '../schemanode';
import { IStringField, StringField } from './string';
export interface IEmailField extends IStringField {
}
export declare class EmailField extends StringField implements IEmailField {
    constructor(f: IEmailField, p?: SchemaNode);
}
