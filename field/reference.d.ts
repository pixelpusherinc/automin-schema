import { SchemaNode } from '../schemanode';
import { IField, Field } from '../field';
import { Entity } from '../entity';
export interface IReferenceField extends IField {
    /** Referenced entity. */
    targetEntity: string;
    /** Field of referenced entity used as foreign key. */
    targetIndex: string;
    /** List of fields to display. */
    targetFields: string[];
    /** Optional format string */
    format?: string;
    /** Optional prompt text */
    promptText?: string;
}
export declare class ReferenceField extends Field {
    format?: string;
    promptText?: string;
    targetEntity: string;
    targetIndex: string;
    targetFields: string[];
    refEntity: Entity;
    refIndex: Field;
    refFields: Field[];
    constructor(f: IReferenceField, p?: SchemaNode);
    resolveReferences(): void;
}
