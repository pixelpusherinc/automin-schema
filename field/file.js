"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var FileField = /** @class */ (function (_super) {
    __extends(FileField, _super);
    function FileField(f, p) {
        var _this = _super.call(this, f, p) || this;
        if (!f.uploadUrl || typeof f.uploadUrl !== 'string') {
            throw new Error("url required for file field " + _this.fullName() + ", must be a string.");
        }
        _this.uploadUrl = f.uploadUrl;
        if (f.uploadName != null) {
            if (typeof f.uploadName !== 'string') {
                throw new Error("uploadName must be a string for file field " + _this.fullName());
            }
        }
        else {
            _this.uploadName = _this.fullName();
        }
        _this.filePath = f.filePath;
        _this.tempPath = f.tempPath;
        if (f.extensions && f.extensions.length > 0) {
            _this.extensions = f.extensions.map(function (e) {
                return (e.charAt(0) === '.') ? e.substr(1) : e;
            });
        }
        return _this;
    }
    return FileField;
}(field_1.Field));
exports.FileField = FileField;
