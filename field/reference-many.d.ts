import { SchemaNode } from '../schemanode';
import { IReferenceField, ReferenceField } from './reference';
export interface IReferenceManyField extends IReferenceField {
    /** Allow duplicate entries or not. Default false. */
    allowDuplicates?: boolean;
}
export declare class ReferenceManyField extends ReferenceField {
    allowDuplicates: boolean;
    constructor(i: IReferenceManyField, p?: SchemaNode);
}
