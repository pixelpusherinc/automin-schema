import { SchemaNode } from '../schemanode';
import { Mode } from '../field';
import { IStringField, StringField } from './string';
import D from '../lib/dict';
export declare type ContentType = 'text' | 'code' | 'json' | 'html';
export declare type Editor = 'plain' | 'tinymce';
/**
 * Multiline text field with rich text editing if requested.
 */
export interface ITextField extends IStringField {
    /** Type of content: 'text' | 'code' | 'html'. Default 'text' */
    contentType?: ContentType;
    /** Type of editor to use. Default 'plain'. */
    editor?: Editor;
    /** Options for editor. (Eg. options passed to TinyMCE API.) */
    editorOptions?: any;
    /** Attempt to validate content */
    validateContent?: boolean;
}
export declare class TextField extends StringField implements ITextField {
    contentType: ContentType;
    editor: Editor;
    editorOptions: any;
    validateContent: boolean;
    constructor(f: ITextField, p?: SchemaNode);
    validateData(data: D<any>, mode: Mode): string | void;
}
