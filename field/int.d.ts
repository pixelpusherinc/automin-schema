import { SchemaNode } from '../schemanode';
import { IField, Field, Mode } from '../field';
import D from '../lib/dict';
export interface IIntField extends IField {
    /** Integer size (not yet implemented) */
    size?: number;
    /** Minimum allowed value. Default any. */
    min?: number;
    /** Maximum allowed value. Default any. */
    max?: number;
    /** Default value to pre-fill on create form. */
    defaultValue?: number;
}
export declare class IntField extends Field implements IIntField {
    size?: number;
    min?: number;
    max?: number;
    defaultValue?: number;
    constructor(f: IIntField, p?: SchemaNode);
    isNumeric(): boolean;
    parseValue(str: string): any;
    validateData(data: D<any>, mode: Mode): string | void;
}
