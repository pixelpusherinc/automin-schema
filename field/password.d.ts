import { SchemaNode } from '../schemanode';
import { IStringField, StringField } from './string';
export interface IPasswordField extends IStringField {
}
export declare class PasswordField extends StringField implements IPasswordField {
    constructor(f: IPasswordField, p?: SchemaNode);
}
