import { SchemaNode } from '../schemanode';
import { IField, Field } from '../field';
export declare const MAX_BIT = 30;
export interface IBitsField extends IField {
    /** Possible bit flags. Bit represents bit position 1..MAX_BIT. Label is used in UI. */
    options: {
        bit: number;
        label: string;
    }[];
}
export declare class BitsField extends Field implements IBitsField {
    options: {
        bit: number;
        label: string;
    }[];
    constructor(f: IBitsField, p?: SchemaNode);
    isNumeric(): boolean;
    parseValue(str: string): any;
    /** Given a bit # (1,2,3,4...) return its corresponding label (else return undefined) */
    bitToLabel(bit: number): string | undefined;
    /** Given a bit # (1,2,3,4...) return the bit value */
    bitToValue(bit: number): number | undefined;
    /** Given an int with various bits set, unpack into array of bit numbers */
    valueToBits(value: number): number[];
    valueToLabels(value: number): string[];
}
