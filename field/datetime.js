"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var DateTimeField = /** @class */ (function (_super) {
    __extends(DateTimeField, _super);
    function DateTimeField(f, p) {
        var _this = _super.call(this, f, p) || this;
        _this.widget = f.widget === 'pikaday' ? 'pikaday' : 'default';
        _this.min = f.min;
        _this.max = f.max;
        _this.minDate = f.min ? new Date(f.min) : new Date('1900-01-01T00:00:00.000Z');
        _this.maxDate = f.max ? new Date(f.max) : new Date('2099-12-31T23:59:59.000Z');
        return _this;
    }
    DateTimeField.prototype.parseValue = function (str) {
        return new Date(str);
    };
    DateTimeField.prototype.validateData = function (data, mode) {
        var err = _super.prototype.validateData.call(this, data, mode);
        if (err)
            return err;
        var val = data[this.name];
        if (val == null || val === '')
            return;
        if (typeof val !== 'string' && !val.getTime) {
            return this.name + " is not a valid type of date.";
        }
        var d = new Date(val);
        if (Number.isNaN(d.getTime())) {
            return this.name + " is not a valid date.";
        }
    };
    /**
     * Format for display in local time.
     */
    DateTimeField.prototype.formatLocal = function (date) {
        // TODO: Use moment (or something) for custom date formatting.
        if (!date || typeof date.toLocaleString !== 'function' ||
            Number.isNaN(date.getTime())) {
            return '';
        }
        var day = date.getDate().toString();
        if (day.length < 2)
            day = '0' + day;
        var h = date.getHours();
        var h12 = (h === 12) ? '12' : (h % 12).toString();
        if (h12.length < 2)
            h12 = '0' + h12;
        var m = date.getMinutes().toString();
        if (m.length < 2)
            m = '0' + m;
        var ap = h < 12 ? 'AM' : 'PM';
        return MONTHS[date.getMonth()] +
            ' ' + day +
            ' ' + date.getFullYear() +
            ' ' + h12 +
            ':' + m +
            ' ' + ap;
    };
    DateTimeField.prototype.formatStringLocal = function (dtStr) {
        return this.formatLocal(new Date(dtStr));
    };
    return DateTimeField;
}(field_1.Field));
exports.DateTimeField = DateTimeField;
