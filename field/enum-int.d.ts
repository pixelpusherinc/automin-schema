import D from '../lib/dict';
import { SchemaNode } from '../schemanode';
import { IField, Field, Mode } from '../field';
export interface IEnumIntField extends IField {
    /** List of possible values and corresponding labels. */
    options: {
        label: string;
        value: number;
    }[];
    promptText?: string;
}
export declare class EnumIntField extends Field implements IEnumIntField {
    options: {
        label: string;
        value: number;
    }[];
    promptText?: string;
    constructor(f: IEnumIntField, p?: SchemaNode);
    valueToLabel(val: number): string;
    isNumeric(): boolean;
    parseValue(str: string): any;
    validateData(data: D<any>, mode: Mode): string | void;
}
