"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var FloatField = /** @class */ (function (_super) {
    __extends(FloatField, _super);
    function FloatField(f, p) {
        var _this = _super.call(this, f, p) || this;
        _this.min = typeof f.min === 'number' ? f.min : undefined;
        _this.max = typeof f.max === 'number' ? f.max : undefined;
        if (f.defaultValue != null) {
            if (typeof f.defaultValue !== 'number') {
                throw new Error("Default value for field '" + _this.fullName() + "' must be a number.");
            }
            _this.defaultValue = f.defaultValue;
        }
        else {
            _this.defaultValue = undefined;
        }
        return _this;
    }
    FloatField.prototype.isNumeric = function () {
        return true;
    };
    FloatField.prototype.parseValue = function (str) {
        return Number(str);
    };
    FloatField.prototype.validateData = function (data, mode) {
        var err = _super.prototype.validateData.call(this, data, mode);
        if (err)
            return err;
        var val = data[this.name];
        if (val == null)
            return;
        if (typeof val !== 'number') {
            return "Invalid type for " + this.label + ".";
        }
        if (Number.isNaN(val)) {
            return "Invalid value for " + this.label + ".";
        }
        if (typeof this.min === 'number' && val < this.min) {
            return this.label + " must be at least " + this.min + ".";
        }
        if (typeof this.max === 'number' && val > this.max) {
            return this.label + " must be no more than " + this.max + ".";
        }
    };
    return FloatField;
}(field_1.Field));
exports.FloatField = FloatField;
