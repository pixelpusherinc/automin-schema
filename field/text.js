"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var string_1 = require("./string");
var TextField = /** @class */ (function (_super) {
    __extends(TextField, _super);
    function TextField(f, p) {
        var _this = _super.call(this, f, p) || this;
        _this.contentType = f.contentType || 'text';
        _this.editor = f.editor || 'plain';
        _this.editorOptions = f.editorOptions;
        _this.validateContent = !!f.validateContent;
        return _this;
    }
    TextField.prototype.validateData = function (data, mode) {
        var err = _super.prototype.validateData.call(this, data, mode);
        if (err)
            return err;
        var val = data[this.name];
        if (this.contentType === 'json' && this.validateContent && val && val.trim().length > 0) {
            try {
                JSON.parse(val);
            }
            catch (e) {
                return this.label + " contains invalid JSON";
            }
        }
    };
    return TextField;
}(string_1.StringField));
exports.TextField = TextField;
