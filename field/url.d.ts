import { SchemaNode } from '../schemanode';
import { IStringField, StringField } from './string';
export interface IUrlField extends IStringField {
}
export declare class UrlField extends StringField implements IUrlField {
    constructor(f: IUrlField, p?: SchemaNode);
}
