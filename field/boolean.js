"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var BooleanField = /** @class */ (function (_super) {
    __extends(BooleanField, _super);
    function BooleanField(f, p) {
        var _this = _super.call(this, f, p) || this;
        if (f.nullable) {
            throw new Error("boolean field " + _this.fullName() + " cannot be nullable");
        }
        _this.nullable = false;
        if (f.defaultValue !== undefined) {
            if (typeof f.defaultValue !== 'boolean') {
                throw new Error("Incorrect type for default value in boolean field " + _this.fullName());
            }
            _this.defaultValue = f.defaultValue;
        }
        return _this;
    }
    BooleanField.prototype.validateData = function (data, mode) {
        // Nothing to validate since data has been parsed as boolean
    };
    return BooleanField;
}(field_1.Field));
exports.BooleanField = BooleanField;
