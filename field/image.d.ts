import { SchemaNode } from '../schemanode';
import { IFileField, FileField } from './file';
export interface IImageField extends IFileField {
}
export declare class ImageField extends FileField implements IImageField {
    constructor(f: IImageField, p?: SchemaNode);
}
