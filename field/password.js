"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var string_1 = require("./string");
var PasswordField = /** @class */ (function (_super) {
    __extends(PasswordField, _super);
    function PasswordField(f, p) {
        var _this = _super.call(this, f, p) || this;
        if (!_this.pattern) {
            _this.pattern = '^\\S*$';
        }
        _this.filterable = f.filterable === true ? true : false;
        return _this;
    }
    return PasswordField;
}(string_1.StringField));
exports.PasswordField = PasswordField;
