import D from '../lib/dict';
import { SchemaNode } from '../schemanode';
import { IField, Field, Mode } from '../field';
export interface IEnumField extends IField {
    /** List of possible values and corresponding labels. */
    options: {
        label: string;
        value: string;
    }[];
    promptText?: string;
}
export declare class EnumField extends Field implements IEnumField {
    options: {
        label: string;
        value: string;
    }[];
    promptText?: string;
    constructor(f: IEnumField, p?: SchemaNode);
    valueToLabel(val: string): string;
    validateData(data: D<any>, mode: Mode): string | void;
}
