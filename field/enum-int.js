"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var EnumIntField = /** @class */ (function (_super) {
    __extends(EnumIntField, _super);
    function EnumIntField(f, p) {
        var _this = _super.call(this, f, p) || this;
        _this.options = f.options.map(function (v) { return ({ label: v.label, value: v.value }); });
        _this.promptText = f.promptText;
        return _this;
    }
    EnumIntField.prototype.valueToLabel = function (val) {
        var o = this.options.find(function (o) { return o.value === val; });
        return o ? o.label : '';
    };
    EnumIntField.prototype.isNumeric = function () {
        return true;
    };
    EnumIntField.prototype.parseValue = function (str) {
        return Number(str);
    };
    EnumIntField.prototype.validateData = function (data, mode) {
        var err = _super.prototype.validateData.call(this, data, mode);
        if (err)
            return err;
        var val = data[this.name];
        if (val == null)
            return;
        if (typeof val !== 'number') {
            return "Invalid type for " + this.label + ".";
        }
        if (Number.isNaN(val) || Math.floor(val) !== val) {
            return "Invalid value for " + this.label + ".";
        }
        if (!this.options.find(function (o) { return o.value === val; })) {
            return "Value does not exist in options for " + this.label;
        }
    };
    return EnumIntField;
}(field_1.Field));
exports.EnumIntField = EnumIntField;
