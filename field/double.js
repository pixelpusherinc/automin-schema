"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var float_1 = require("./float");
var DoubleField = /** @class */ (function (_super) {
    __extends(DoubleField, _super);
    function DoubleField(f, p) {
        return _super.call(this, f, p) || this;
    }
    return DoubleField;
}(float_1.FloatField));
exports.DoubleField = DoubleField;
