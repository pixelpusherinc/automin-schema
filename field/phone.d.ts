import { SchemaNode } from '../schemanode';
import { IStringField, StringField } from './string';
export interface IPhoneField extends IStringField {
}
export declare class PhoneField extends StringField implements IPhoneField {
    constructor(f: IPhoneField, p?: SchemaNode);
}
