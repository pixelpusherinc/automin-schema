import { SchemaNode } from '../schemanode';
import { IField, Field, Mode } from '../field';
import D from '../lib/dict';
export declare type DateTimeWidgetType = 'default' | 'pikaday';
export interface IDateTimeField extends IField {
    /** Which datetime input widget to use: 'default' (built-in) or 'pikaday' */
    widget?: DateTimeWidgetType;
    /** Minimum date string */
    min?: string;
    /** Maximum date string */
    max?: string;
}
export declare class DateTimeField extends Field implements IDateTimeField {
    widget: DateTimeWidgetType;
    min?: string;
    max?: string;
    minDate: Date;
    maxDate: Date;
    constructor(f: IDateTimeField, p?: SchemaNode);
    parseValue(str: string): any;
    validateData(data: D<any>, mode: Mode): string | void;
    /**
     * Format for display in local time.
     */
    formatLocal(date: Date): string;
    formatStringLocal(dtStr: string): string;
}
