import { SchemaNode } from '../schemanode';
import { IField, Field, Mode } from '../field';
import D from '../lib/dict';
/** Base type for string/text fields. */
export interface IStringField extends IField {
    /** Maximum length for string. Default any. */
    size?: number;
    /** Regular expression that input will be validated against. */
    pattern?: string;
    /** Trim whitespace of strings on submit. Default true. */
    trim?: boolean;
    /** Default value to pre-fill on create form. */
    defaultValue?: string;
}
export declare class StringField extends Field implements IStringField {
    size?: number;
    pattern?: string;
    regex?: RegExp;
    trim: boolean;
    defaultValue?: string;
    constructor(f: IStringField, p?: SchemaNode);
    validateData(data: D<any>, mode: Mode): string | void;
}
