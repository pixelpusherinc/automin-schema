"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var reference_1 = require("./reference");
var ReferenceManyField = /** @class */ (function (_super) {
    __extends(ReferenceManyField, _super);
    function ReferenceManyField(i, p) {
        var _this = _super.call(this, i, p) || this;
        _this.allowDuplicates = !!i.allowDuplicates;
        return _this;
    }
    return ReferenceManyField;
}(reference_1.ReferenceField));
exports.ReferenceManyField = ReferenceManyField;
