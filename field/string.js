"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var StringField = /** @class */ (function (_super) {
    __extends(StringField, _super);
    function StringField(f, p) {
        var _this = _super.call(this, f, p) || this;
        if (f.size != null && typeof f.size !== 'number') {
            throw new Error("size for field '" + _this.fullName() + "' must be a number.");
        }
        _this.size = f.size && (f.size > 0) ? f.size : undefined;
        _this.pattern = f.pattern || undefined;
        _this.regex = _this.pattern ? new RegExp(_this.pattern) : undefined;
        _this.trim = !(f.trim === false);
        if (f.defaultValue != null) {
            if (typeof f.defaultValue !== 'string') {
                throw new Error("defaultValue for field '" + _this.fullName() + "' must be a string.");
            }
            _this.defaultValue = f.defaultValue;
        }
        else {
            _this.defaultValue = undefined;
        }
        return _this;
    }
    StringField.prototype.validateData = function (data, mode) {
        var err = _super.prototype.validateData.call(this, data, mode);
        if (err)
            return err;
        var val = data[this.name];
        if (val == null)
            return;
        if (typeof val !== 'string') {
            return "Invalid type for " + this.label + ".";
        }
        if (typeof this.size === 'number' && val.length > this.size) {
            return this.label + " cannot be longer than " + this.size + " characters.";
        }
        if (this.regex) {
            if (!this.regex.test(val)) {
                return "Invalid format for " + this.label + ".";
            }
        }
    };
    return StringField;
}(field_1.Field));
exports.StringField = StringField;
