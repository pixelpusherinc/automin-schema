"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var schemanode_1 = require("../schemanode");
var dict_1 = require("../lib/dict");
var Field = /** @class */ (function (_super) {
    __extends(Field, _super);
    function Field(i, p) {
        var _this = _super.call(this, i, p) || this;
        _this.type = i.type;
        _this.primaryKey = !!i.primaryKey;
        _this.nullable = !(i.nullable === false);
        _this.required = !!i.required;
        _this.unique = !!i.unique;
        _this.autoCreated = !!i.autoCreated;
        _this.autoUpdated = !!i.autoUpdated;
        _this.lockedAfterCreate = !!i.lockedAfterCreate;
        _this.filterable = !(i.filterable === false);
        _this.helpText = i.helpText;
        _this.helpHtml = i.helpHtml;
        return _this;
    }
    Field.create = function (i, p) {
        if (!dict_1.default.has(exports.types, i.type)) {
            throw new Error("Cannot create field of unknown type: '" + i.type + "'");
        }
        return new exports.types[i.type](i, p);
    };
    Field.prototype.updateable = function () {
        return !(this.autoUpdated || this.lockedAfterCreate);
    };
    Field.prototype.isNumeric = function () {
        return false;
    };
    Field.prototype.parseValue = function (str) {
        return str;
    };
    /** After form inputs have been parsed/submitted, validate. */
    Field.prototype.validateData = function (data, mode) {
        if (mode === 'create' && this.autoCreated)
            return;
        if (mode === 'edit' && (!this.updateable() || !dict_1.default.has(data, this.name)))
            return;
        var val = data[this.name];
        if (!this.nullable && val == null) {
            return this.label + " cannot be null";
        }
        if (this.required && (val == null || val === '')) {
            return this.label + " is required.";
        }
    };
    return Field;
}(schemanode_1.SchemaNode));
exports.Field = Field;
//
// Now that Field base type has been declared, we can
// register all known Field sub-classes
//
var boolean_1 = require("./boolean");
var string_1 = require("./string");
var email_1 = require("./email");
var url_1 = require("./url");
var password_1 = require("./password");
var phone_1 = require("./phone");
var text_1 = require("./text");
var int_1 = require("./int");
var long_1 = require("./long");
var float_1 = require("./float");
var double_1 = require("./double");
var order_1 = require("./order");
var enum_1 = require("./enum");
var enum_int_1 = require("./enum-int");
var bits_1 = require("./bits");
var datetime_1 = require("./datetime");
var file_1 = require("./file");
var image_1 = require("./image");
var reference_1 = require("./reference");
var reference_many_1 = require("./reference-many");
exports.types = {
    boolean: boolean_1.BooleanField,
    int: int_1.IntField,
    long: long_1.LongField,
    float: float_1.FloatField,
    double: double_1.DoubleField,
    order: order_1.OrderField,
    string: string_1.StringField,
    email: email_1.EmailField,
    url: url_1.UrlField,
    password: password_1.PasswordField,
    phone: phone_1.PhoneField,
    text: text_1.TextField,
    enum: enum_1.EnumField,
    enumInt: enum_int_1.EnumIntField,
    bits: bits_1.BitsField,
    datetime: datetime_1.DateTimeField,
    file: file_1.FileField,
    image: image_1.ImageField,
    reference: reference_1.ReferenceField,
    referenceMany: reference_many_1.ReferenceManyField
};
