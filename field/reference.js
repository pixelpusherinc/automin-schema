"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var ReferenceField = /** @class */ (function (_super) {
    __extends(ReferenceField, _super);
    function ReferenceField(f, p) {
        var _this = _super.call(this, f, p) || this;
        if (f.targetEntity == null) {
            throw new Error(_this.fullName() + " targetEntity required.");
        }
        if (typeof f.targetEntity !== 'string') {
            throw new Error(_this.fullName() + " targetEntity must be a string.");
        }
        _this.targetEntity = f.targetEntity;
        if (f.targetIndex == null) {
            throw new Error(_this.fullName() + " targetIndex required.");
        }
        if (typeof f.targetIndex !== 'string') {
            throw new Error(_this.fullName() + " targetIndex must be a string.");
        }
        _this.targetIndex = f.targetIndex;
        if (f.targetFields == null) {
            throw new Error(_this.fullName() + " targetFields required.");
        }
        if (!Array.isArray(f.targetFields)) {
            throw new Error(_this.fullName() + " targetFields must be an array of field names.");
        }
        _this.targetFields = f.targetFields;
        if (f.format !== undefined && typeof f.format !== 'string') {
            throw new Error(_this.fullName() + " format property must be a string.");
        }
        _this.format = f.format;
        if (f.promptText !== undefined && typeof f.promptText !== 'string') {
            throw new Error(_this.fullName() + " must be a string.");
        }
        _this.promptText = f.promptText;
        return _this;
    }
    ReferenceField.prototype.resolveReferences = function () {
        var _this = this;
        var s = this.getRoot();
        var ent;
        var field;
        if (!(ent = s.getEntity(this.targetEntity))) {
            throw new Error("Couldn't resolve reference to entity '" + this.targetEntity + "' for field " + this.fullName());
        }
        this.refEntity = ent;
        if (!(field = this.refEntity.getField(this.targetIndex))) {
            throw new Error("Couldn't resolve reference to index field '" + this.targetIndex + "' for field " + this.fullName());
        }
        this.refIndex = field;
        this.refFields = [];
        if (!this.targetFields || this.targetFields.length < 1) {
            throw new Error("One or more targetFields required for field " + this.fullName());
        }
        this.targetFields.forEach(function (fname) {
            var f = _this.refEntity.getField(fname);
            if (!f) {
                throw new Error("Couldn't resolve reference to targetField '" + f + "' for field " + _this.fullName());
            }
            _this.refFields.push(f);
        });
    };
    return ReferenceField;
}(field_1.Field));
exports.ReferenceField = ReferenceField;
