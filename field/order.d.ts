import { SchemaNode } from '../schemanode';
import { IField, Field, Mode } from '../field';
import D from '../lib/dict';
export interface IOrderField extends IField {
}
export declare class OrderField extends Field implements IOrderField {
    constructor(f: IOrderField, p?: SchemaNode);
    isNumeric(): boolean;
    parseValue(str: string): any;
    validateData(data: D<any>, mode: Mode): string | void;
}
