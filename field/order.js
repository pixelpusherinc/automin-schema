"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var OrderField = /** @class */ (function (_super) {
    __extends(OrderField, _super);
    function OrderField(f, p) {
        var _this = _super.call(this, f, p) || this;
        _this.nullable = false;
        _this.required = true;
        _this.unique = true;
        _this.autoCreated = true;
        _this.autoUpdated = true;
        _this.lockedAfterCreate = true;
        return _this;
    }
    OrderField.prototype.isNumeric = function () {
        return true;
    };
    OrderField.prototype.parseValue = function (str) {
        return Number(str);
    };
    OrderField.prototype.validateData = function (data, mode) {
        // Assume always ok because user cannot change directly and
        // this value should be omitted from create/update requests.
        return;
    };
    return OrderField;
}(field_1.Field));
exports.OrderField = OrderField;
