"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var IntField = /** @class */ (function (_super) {
    __extends(IntField, _super);
    function IntField(f, p) {
        var _this = _super.call(this, f, p) || this;
        _this.size = typeof f.size === 'number' ? Math.round(f.size) : undefined;
        _this.min = typeof f.min === 'number' ? Math.round(f.min) : undefined;
        _this.max = typeof f.max === 'number' ? Math.round(f.max) : undefined;
        if (f.defaultValue != null) {
            if (typeof f.defaultValue !== 'number' || Math.floor(f.defaultValue) !== f.defaultValue) {
                throw new Error("Default value for field '" + _this.fullName() + "' must be an integer.");
            }
            _this.defaultValue = f.defaultValue;
        }
        else {
            _this.defaultValue = undefined;
        }
        return _this;
    }
    IntField.prototype.isNumeric = function () {
        return true;
    };
    IntField.prototype.parseValue = function (str) {
        return Number(str);
    };
    IntField.prototype.validateData = function (data, mode) {
        var err = _super.prototype.validateData.call(this, data, mode);
        if (err)
            return err;
        var val = data[this.name];
        if (val == null)
            return;
        if (typeof val !== 'number') {
            return "Invalid type for " + this.label + ".";
        }
        if (Number.isNaN(val) || Math.floor(val) !== val) {
            return "Invalid value for " + this.label + ".";
        }
        if (typeof this.min === 'number' && val < this.min) {
            return this.label + " must be at least " + this.min + ".";
        }
        if (typeof this.max === 'number' && val > this.max) {
            return this.label + " must be no more than " + this.max + ".";
        }
    };
    return IntField;
}(field_1.Field));
exports.IntField = IntField;
