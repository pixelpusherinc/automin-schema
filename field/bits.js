"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("../field");
var dict_1 = require("../lib/dict");
exports.MAX_BIT = 30;
var BitsField = /** @class */ (function (_super) {
    __extends(BitsField, _super);
    function BitsField(f, p) {
        var _this = _super.call(this, f, p) || this;
        var fullName = _this.fullName();
        if (_this.nullable) {
            throw new Error("bits field " + fullName + " cannot be nullable");
        }
        var usedBits = dict_1.default();
        _this.options = f.options.map(function (o) {
            if (typeof o.bit !== 'number') {
                throw new Error("bits field " + fullName + " bit value must be a number");
            }
            if (o.bit < 1 || o.bit > exports.MAX_BIT || o.bit !== Math.floor(o.bit)) {
                throw new Error("bits field " + fullName + " bit values must be integer from 1-" + exports.MAX_BIT);
            }
            if (typeof o.label !== 'string') {
                throw new Error("bits field " + fullName + " label must be a string");
            }
            var bs = o.bit.toString();
            if (usedBits[bs]) {
                throw new Error("bits field " + fullName + " has duplicate bit value: " + o.bit);
            }
            usedBits[bs] = true;
            return {
                bit: o.bit, label: o.label
            };
        });
        return _this;
    }
    BitsField.prototype.isNumeric = function () {
        return true;
    };
    BitsField.prototype.parseValue = function (str) {
        return Number.parseInt(str);
    };
    /** Given a bit # (1,2,3,4...) return its corresponding label (else return undefined) */
    BitsField.prototype.bitToLabel = function (bit) {
        var o = this.options.find(function (o) { return o.bit === bit; });
        return o ? o.label : undefined;
    };
    /** Given a bit # (1,2,3,4...) return the bit value */
    BitsField.prototype.bitToValue = function (bit) {
        if (typeof bit !== 'number' || bit < 1 || bit > exports.MAX_BIT) {
            throw new Error("Invalid bit value: " + bit + " for field " + this.fullName());
        }
        var o = this.options.find(function (o) { return o.bit === bit; });
        if (!o) {
            throw new Error("Unknown bit value: " + bit + " for field " + this.fullName());
        }
        return 1 << (bit - 1);
    };
    /** Given an int with various bits set, unpack into array of bit numbers */
    BitsField.prototype.valueToBits = function (value) {
        var bits = [];
        for (var i = 0, n = this.options.length; i < n; ++i) {
            var b = this.options[i].bit;
            var bv = 1 << (b - 1);
            if (value & bv) {
                bits.push(b);
            }
        }
        return bits;
    };
    BitsField.prototype.valueToLabels = function (value) {
        var labels = [];
        for (var i = 0, n = this.options.length; i < n; ++i) {
            var o = this.options[i];
            var bv = 1 << (o.bit - 1);
            if (value & bv) {
                labels.push(o.label);
            }
        }
        return labels;
    };
    return BitsField;
}(field_1.Field));
exports.BitsField = BitsField;
