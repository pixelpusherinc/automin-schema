import {ISchemaNode, SchemaNode} from '../schemanode'
import {IField, Field, Mode} from '../field'
import {OrderField} from '../field/order'
import D from '../lib/dict'

export interface Filter {
	fieldName: string
	text: string
}

export interface Sort {
	field: string
	order: 'ASC' | 'DESC'
}

/**
 * Entity represents a DB table.
 */
export interface IEntity extends ISchemaNode {
	/** If provided, will be used rather than attempting to automatically pluralize label. */
	labelPlural?: string
	/** List of fields in this entity */
	fields: IField[]
	/** Optional list of fields to display in list views. Defaults to displaying all. */
	listViewFields?: string[]
	/** Number of rows displayed per page. */
	pageRowCount?: number
	/** Default list sort. Array of {field: string, order: 'ASC' | 'DESC'} */
	defaultSort?: Sort[]
}

export class Entity extends SchemaNode implements IEntity {
	labelPlural: string
	fields: Field[]
	listViewFields: string[]
	pageRowCount: number
	defaultSort: Sort[]
	orderField?: OrderField

	constructor (i: IEntity, p?: SchemaNode) {
		super(i, p)
		this.labelPlural = i.labelPlural ?
			i.labelPlural : Entity.pluralize(this.label)
		// Fields...
		this.orderField = undefined
		let pkeys = 0
		this.fields = i.fields.map(f => {
			if (f.primaryKey) {
				if (pkeys > 1) {
					throw new Error(`Entity '${this.name}' cannot have more than one primary key.`)
				}
				pkeys++
			}
			if (f.type === 'order') {
				if (this.orderField) {
					throw new Error(`Entity '${this.name} cannot have more than one order field.`)
				}
			}
			const field = Field.create(f, this)
			if (field.type === 'order') {
				this.orderField = field as OrderField
			}
			return field
		})
		if (i.listViewFields) {
			// Schema has specified fields to appear in list view
			this.listViewFields = i.listViewFields.map(f => {
				if (!this.getField(f)) {
					throw new Error(`Field '${f}' in listViewFields for entity '${this.name}' does not exist.`)
				}
				return f
			})
		} else {
			// Use all fields in list view
			this.listViewFields = i.fields.map(f => f.name)
		}
		this.pageRowCount = i.pageRowCount || 25
		if (i.defaultSort) {
			this.defaultSort = i.defaultSort
		} else {
			if (this.orderField) {
				this.defaultSort = [{field: this.orderField!.name, order: 'ASC'}]
			} else {
				this.defaultSort = [{field: this.getPrimaryKey().name, order: 'ASC'}]
			}
		}
	}

	/**
	 * Find a field in this entity by name.
	 */
	getField (fname: string) {
		return this.fields.find(f => f.name === fname)
	}

	/**
	 * Returns the primary key field.
	 */
	getPrimaryKey() {
		return this.fields.find(f => !!f.primaryKey) as Field
	}

	/**
	 * Returns array of fields to show in list view.
	 */
	getListFields() {
		let fields: Field[]
		if (this.listViewFields && this.listViewFields.length > 0) {
			fields = this.listViewFields.map(fname => this.getField(fname) as Field)
		} else {
			fields = this.fields
		}
		return fields
	}

	/**
	 * Determine if this entity has any updateable fields
	 */
	updateable(): boolean {
		return !!this.fields.find(f => f.updateable())
	}

	resolveReferences() {
		this.fields.forEach(f => {
			f.resolveReferences()
		})
	}

	/**
	 * Attempts to validate a record (for update/create)
	 */
	validateData (data: D<any>, mode: Mode): D<string> | void {
		if (mode !== 'create' && mode !== 'edit') {
			throw new Error("Incorrect mode for validateData: " + mode)
		}
		const errors = D<string>()
		this.fields.forEach(field => {
			const err = field.validateData(data, mode)
			if (err) {
				errors[field.name] = err
			}
		})
		return Object.keys(errors).length > 0 ? errors : undefined
	}

	static create (i: IEntity, p?: SchemaNode) {
		return new Entity(i, p)
	}

	static pluralize (s: string) {
		let p = s
		s = s.toLowerCase()
		if (s.endsWith('s')) {
			// nothing to do
		} else if (s.endsWith('y') && !s.endsWith('ey')) {
			p = p.substr(0, p.length - 1) + 'ies'
		} else {
			p += 's'
		}
		return p
	}
}
