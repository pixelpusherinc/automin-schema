/**
 * Possible actions that can be performed on a schema object
 */
export type Action = 'show' | 'create' | 'edit' | 'delete'

/**
 * Base type for all schema nodes
 */
export interface ISchemaNode {
	/** Name for this node. Must be unique within its scope. */
	name: string
	/** Human-friendly label for this node. Uses name if not supplied. */
	label?: string
	/** Optional description text. */
	description?: string
	/** Optional list of allowed actions. Defaults to all. */
	allowedActions?: Action[]
}

export abstract class SchemaNode implements ISchemaNode {
	name: string
	label: string
	description: string
	allowedActions?: Action[]
	parent: SchemaNode | undefined

	constructor (i: ISchemaNode, parent?: SchemaNode) {
		this.parent = parent || undefined
		this.name = validateName(i.name, parent)
		this.label = i.label || this.name
		this.description = i.description || ''

		if (i.allowedActions != null) {
			if (!Array.isArray(i.allowedActions)) {
				throw new Error(`allowedActions for '${this.fullName()}' must be an array`)
			}
			i.allowedActions.forEach(a => {
				if (a !== 'show' && a !== 'create' && a !== 'edit' && a !== 'delete') {
					throw new Error(`Unrecognized action in allowedActions for '${this.fullName()}'`)
				}
			})
			this.allowedActions = i.allowedActions
		}
	}

	/** Returns the root node of this schema tree */
	getRoot(): SchemaNode {
		let p: SchemaNode = this
		while (!!p.parent) p = p.parent
		return p
	}

	/** For 2nd pass after schema parsed */
	resolveReferences() {}

	fullName() {
		return this.parent ? this.parent.name + '.' + this.name : this.name
	}

	isAllowed (action: Action) {
		for (let p = this as SchemaNode | undefined; p != null; p = p.parent) {
			if (p.allowedActions != null && p.allowedActions.indexOf(action) < 0) {
				return false
			}
		}
		return true
	}
}

function validateName (name: string, parent?: SchemaNode) {
	const strParent = parent ? " for child of " + parent.name : ""
	if (!name) {
		throw new Error("A name is required" + strParent)
	}
	if (typeof name !== 'string') {
		throw new Error("name property must be a string" + strParent)
	}
	if (name.trim() !== name) {
		throw new Error("name property cannot start or end with spaces" + strParent)
	}
	return name
}
