import {SchemaNode} from '../schemanode'
import {IField, Field, Mode} from '../field'
import D from '../lib/dict'

export interface IOrderField extends IField {}

export class OrderField extends Field implements IOrderField {
	constructor (f: IOrderField, p?: SchemaNode) {
		super(f, p)
		this.nullable = false
		this.required = true
		this.unique = true
		this.autoCreated = true
		this.autoUpdated = true
		this.lockedAfterCreate = true
	}

	isNumeric(): boolean {
		return true
	}

	parseValue (str: string): any {
		return Number(str)
	}

	validateData (data: D<any>, mode: Mode): string | void {
		// Assume always ok because user cannot change directly and
		// this value should be omitted from create/update requests.
		return
	}
}
