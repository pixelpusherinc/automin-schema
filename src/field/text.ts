import {SchemaNode} from '../schemanode'
import {Mode} from '../field'
import {IStringField, StringField} from './string'
import D from '../lib/dict'

export type ContentType = 'text' | 'code' | 'json' | 'html'
export type Editor = 'plain' | 'tinymce'

/**
 * Multiline text field with rich text editing if requested.
 */
export interface ITextField extends IStringField {
	/** Type of content: 'text' | 'code' | 'html'. Default 'text' */
	contentType?: ContentType
	/** Type of editor to use. Default 'plain'. */
	editor?: Editor
	/** Options for editor. (Eg. options passed to TinyMCE API.) */
	editorOptions?: any
	/** Attempt to validate content */
	validateContent?: boolean
}

export class TextField extends StringField implements ITextField {
	contentType: ContentType
	editor: Editor
	editorOptions: any
	validateContent: boolean
	constructor (f: ITextField, p?: SchemaNode) {
		super(f, p)
		this.contentType = f.contentType || 'text'
		this.editor = f.editor || 'plain'
		this.editorOptions = f.editorOptions
		this.validateContent = !!f.validateContent
	}

	validateData (data: D<any>, mode: Mode): string | void {
		const err = super.validateData(data, mode)
		if (err) return err
		const val = data[this.name]
		if (this.contentType === 'json' && this.validateContent && val && val.trim().length > 0) {
			try {
				JSON.parse(val)
			} catch (e) {
				return `${this.label} contains invalid JSON`
			}
		}
	}
}
