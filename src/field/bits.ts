import {SchemaNode} from '../schemanode'
import {IField, Field} from '../field'
import D from '../lib/dict'

export const MAX_BIT = 30

export interface IBitsField extends IField {
	/** Possible bit flags. Bit represents bit position 1..MAX_BIT. Label is used in UI. */
	options: {bit: number, label: string}[]
}

export class BitsField extends Field implements IBitsField {
	options: {bit: number, label: string}[]
	constructor (f: IBitsField, p?: SchemaNode) {
		super(f, p)
		const fullName = this.fullName()
		if (this.nullable) {
			throw new Error(`bits field ${fullName} cannot be nullable`)
		}
		const usedBits = D<boolean>()
		this.options = f.options.map(o => {
			if (typeof o.bit !== 'number') {
				throw new Error(`bits field ${fullName} bit value must be a number`)
			}
			if (o.bit < 1 || o.bit > MAX_BIT || o.bit !== Math.floor(o.bit)) {
				throw new Error(`bits field ${fullName} bit values must be integer from 1-${MAX_BIT}`)
			}
			if (typeof o.label !== 'string') {
				throw new Error(`bits field ${fullName} label must be a string`)
			}
			const bs = o.bit.toString()
			if (usedBits[bs]) {
				throw new Error(`bits field ${fullName} has duplicate bit value: ${o.bit}`)
			}
			usedBits[bs] = true
			return {
				bit: o.bit, label: o.label
			}
		})
	}

	isNumeric(): boolean {
		return true
	}

	parseValue (str: string): any {
		return Number.parseInt(str)
	}

	/** Given a bit # (1,2,3,4...) return its corresponding label (else return undefined) */
	bitToLabel (bit: number): string | undefined {
		const o = this.options.find(o => o.bit === bit)
		return o ? o.label : undefined
	}

	/** Given a bit # (1,2,3,4...) return the bit value */
	bitToValue (bit: number): number | undefined {
		if (typeof bit !== 'number' || bit < 1 || bit > MAX_BIT) {
			throw new Error(`Invalid bit value: ${bit} for field ${this.fullName()}`)
		}
		const o = this.options.find(o => o.bit === bit)
		if (!o) {
			throw new Error(`Unknown bit value: ${bit} for field ${this.fullName()}`)
		}
		return 1 << (bit - 1)
	}

	/** Given an int with various bits set, unpack into array of bit numbers */
	valueToBits (value: number): number[] {
		const bits: number[] = []
		for (let i = 0, n = this.options.length; i < n; ++i) {
			const b = this.options[i].bit
			const bv = 1 << (b - 1)
			if (value & bv) {
				bits.push(b)
			}
		}
		return bits
	}

	valueToLabels (value: number): string[] {
		const labels: string[] = []
		for (let i = 0, n = this.options.length; i < n; ++i) {
			const o = this.options[i]
			const bv = 1 << (o.bit - 1)
			if (value & bv) {
				labels.push(o.label)
			}
		}
		return labels
	}
}
