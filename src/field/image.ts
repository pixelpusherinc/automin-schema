import {SchemaNode} from '../schemanode'
import {IFileField, FileField} from './file'

export interface IImageField extends IFileField {}

export class ImageField extends FileField implements IImageField {
	constructor(f: IImageField, p?: SchemaNode) {
		super(f, p)
	}
}
