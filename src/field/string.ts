import {SchemaNode} from '../schemanode'
import {IField, Field, Mode} from '../field'
import D from '../lib/dict'

/** Base type for string/text fields. */
export interface IStringField extends IField {
	/** Maximum length for string. Default any. */
	size?: number
	/** Regular expression that input will be validated against. */
	pattern?: string
	/** Trim whitespace of strings on submit. Default true. */
	trim?: boolean
	/** Default value to pre-fill on create form. */
	defaultValue?: string
}

export class StringField extends Field implements IStringField {
	size?: number
	pattern?: string
	regex?: RegExp
	trim: boolean
	defaultValue?: string
	constructor (f: IStringField, p?: SchemaNode) {
		super(f, p)
		if (f.size != null && typeof f.size !== 'number') {
			throw new Error(`size for field '${this.fullName()}' must be a number.`)
		}
		this.size = f.size && (f.size > 0) ? f.size : undefined
		this.pattern = f.pattern || undefined
		this.regex = this.pattern ? new RegExp(this.pattern) : undefined
		this.trim = !(f.trim === false)
		if (f.defaultValue != null) {
			if (typeof f.defaultValue !== 'string') {
				throw new Error(`defaultValue for field '${this.fullName()}' must be a string.`)
			}
			this.defaultValue = f.defaultValue
		} else {
			this.defaultValue = undefined
		}
	}

	validateData (data: D<any>, mode: Mode): string | void {
		const err = super.validateData(data, mode)
		if (err) return err
		const val = data[this.name] as string
		if (val == null) return
		if (typeof val !== 'string') {
			return `Invalid type for ${this.label}.`
		}
		if (typeof this.size === 'number' && val.length > this.size) {
			return `${this.label} cannot be longer than ${this.size} characters.`
		}
		if (this.regex) {
			if (!this.regex.test(val)) {
				return `Invalid format for ${this.label}.`
			}
		}
	}
}
