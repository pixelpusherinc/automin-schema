import D from '../lib/dict'
import {SchemaNode} from '../schemanode'
import {IField, Field, Mode} from '../field'

export interface IEnumIntField extends IField {
	/** List of possible values and corresponding labels. */
	options: {label: string, value: number}[]
	promptText?: string
}

export class EnumIntField extends Field implements IEnumIntField {
	options: {label: string, value: number}[]
	promptText?: string
	constructor (f: IEnumIntField, p?: SchemaNode) {
		super(f, p)
		this.options = f.options.map(
			v => ({label: v.label, value: v.value})
		)
		this.promptText = f.promptText
	}

	valueToLabel (val: number) {
		const o = this.options.find(o => o.value === val)
		return o ? o.label : ''
	}

	isNumeric(): boolean {
		return true
	}

	parseValue (str: string): any {
		return Number(str)
	}

	validateData (data: D<any>, mode: Mode): string | void {
		const err = super.validateData(data, mode)
		if (err) return err
		const val = data[this.name]
		if (val == null) return
		if (typeof val !== 'number') {
			return `Invalid type for ${this.label}.`
		}
		if (Number.isNaN(val) || Math.floor(val) !== val) {
			return `Invalid value for ${this.label}.`
		}
		if (!this.options.find(o => o.value === val)) {
			return `Value does not exist in options for ${this.label}`
		}
	}
}
