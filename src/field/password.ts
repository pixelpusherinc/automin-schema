import {SchemaNode} from '../schemanode'
import {IStringField, StringField} from './string'

export interface IPasswordField extends IStringField {}

export class PasswordField extends StringField implements IPasswordField {
	constructor(f: IPasswordField, p?: SchemaNode) {
		super(f, p)
		if (!this.pattern) {
			this.pattern = '^\\S*$'
		}
		this.filterable = f.filterable === true ? true : false
	}
}
