import {SchemaNode} from '../schemanode'
import {IStringField, StringField} from './string'

export interface IEmailField extends IStringField {}

export class EmailField extends StringField implements IEmailField {
	constructor(f: IEmailField, p?: SchemaNode) {
		super(f, p)
		if (!this.pattern) {
			this.pattern = '^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$'
		}
	}
}
