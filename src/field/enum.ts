import D from '../lib/dict'
import {SchemaNode} from '../schemanode'
import {IField, Field, Mode} from '../field'

export interface IEnumField extends IField {
	/** List of possible values and corresponding labels. */
	options: {label: string, value: string}[]
	promptText?: string
}

export class EnumField extends Field implements IEnumField {
	options: {label: string, value: string}[]
	promptText?: string
	constructor (f: IEnumField, p?: SchemaNode) {
		super(f, p)
		this.options = f.options.map(
			v => ({label: v.label, value: v.value})
		)
		this.promptText = f.promptText
	}

	valueToLabel (val: string) {
		const o = this.options.find(o => o.value === val)
		return o ? o.label : ''
	}

	validateData (data: D<any>, mode: Mode): string | void {
		const err = super.validateData(data, mode)
		if (err) return err
		const val = data[this.name]
		if (val == null) return
		if (typeof val !== 'string') {
			return `Invalid type for ${this.label}.`
		}
		if (!this.options.find(o => o.value === val)) {
			return `Value does not exist in options for ${this.label}`
		}
	}
}
