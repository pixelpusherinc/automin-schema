import {SchemaNode} from '../schemanode'
import {IStringField, StringField} from './string'

export interface IUrlField extends IStringField {}

export class UrlField extends StringField implements IUrlField {
	constructor(f: IUrlField, p?: SchemaNode) {
		super(f, p)
	}
}
