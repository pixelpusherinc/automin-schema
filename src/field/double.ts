import {SchemaNode} from '../schemanode'
import {IFloatField, FloatField} from './float'

export interface IDoubleField extends IFloatField {}

export class DoubleField extends FloatField implements IDoubleField {
	constructor(f: IDoubleField, p?: SchemaNode) {
		super(f, p)
	}
}
