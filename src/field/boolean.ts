import {SchemaNode} from '../schemanode'
import {IField, Field, Mode} from '../field'
import D from '../lib/dict'

export interface IBooleanField extends IField {
	defaultValue?: boolean
}

export class BooleanField extends Field implements IBooleanField {
	defaultValue?: boolean
	constructor (f: IBooleanField, p?: SchemaNode) {
		super(f, p)
		if (f.nullable) {
			throw new Error(`boolean field ${this.fullName()} cannot be nullable`)
		}
		this.nullable = false
		if (f.defaultValue !== undefined) {
			if (typeof f.defaultValue !== 'boolean') {
				throw new Error(`Incorrect type for default value in boolean field ${this.fullName()}`)
			}
			this.defaultValue = f.defaultValue
		}
	}

	validateData (data: D<any>, mode: Mode): string | void {
		// Nothing to validate since data has been parsed as boolean
	}
}
