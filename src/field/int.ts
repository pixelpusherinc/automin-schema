import {SchemaNode} from '../schemanode'
import {IField, Field, Mode} from '../field'
import D from '../lib/dict'

export interface IIntField extends IField {
	/** Integer size (not yet implemented) */
	size?: number
	/** Minimum allowed value. Default any. */
	min?: number
	/** Maximum allowed value. Default any. */
	max?: number
	/** Default value to pre-fill on create form. */
	defaultValue?: number
}

export class IntField extends Field implements IIntField {
	size?: number
	min?: number
	max?: number
	defaultValue?: number

	constructor (f: IIntField, p?: SchemaNode) {
		super(f, p)
		this.size = typeof f.size === 'number' ? Math.round(f.size) : undefined
		this.min = typeof f.min === 'number' ? Math.round(f.min) : undefined
		this.max = typeof f.max === 'number' ? Math.round(f.max) : undefined
		if (f.defaultValue != null) {
			if (typeof f.defaultValue !== 'number' || Math.floor(f.defaultValue) !== f.defaultValue) {
				throw new Error(`Default value for field '${this.fullName()}' must be an integer.`)
			}
			this.defaultValue = f.defaultValue
		} else {
			this.defaultValue = undefined
		}
	}

	isNumeric(): boolean {
		return true
	}

	parseValue (str: string): any {
		return Number(str)
	}

	validateData (data: D<any>, mode: Mode): string | void {
		const err = super.validateData(data, mode)
		if (err) return err
		const val = data[this.name]
		if (val == null) return
		if (typeof val !== 'number') {
			return `Invalid type for ${this.label}.`
		}
		if (Number.isNaN(val) || Math.floor(val) !== val) {
			return `Invalid value for ${this.label}.`
		}
		if (typeof this.min === 'number' && val < this.min) {
			return `${this.label} must be at least ${this.min}.`
		}
		if (typeof this.max === 'number' && val > this.max) {
			return `${this.label} must be no more than ${this.max}.`
		}
	}
}
