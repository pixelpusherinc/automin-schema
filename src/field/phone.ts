import {SchemaNode} from '../schemanode'
import {IStringField, StringField} from './string'

export interface IPhoneField extends IStringField {}

export class PhoneField extends StringField implements IPhoneField {
	constructor (f: IPhoneField, p?: SchemaNode) {
		super(f, p)
		if (!this.pattern) {
			this.pattern = '(\\D*\\d){10}'
		}
	}
}
