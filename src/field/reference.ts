import {SchemaNode} from '../schemanode'
import {IField, Field} from '../field'
import {Entity} from '../entity'
import {Schema} from '../'

export interface IReferenceField extends IField {
	/** Referenced entity. */
	targetEntity: string
	/** Field of referenced entity used as foreign key. */
	targetIndex: string
	/** List of fields to display. */
	targetFields: string[]
	/** Optional format string */
	format?: string
	/** Optional prompt text */
	promptText?: string
}

export class ReferenceField extends Field {
	format?: string
	promptText?: string
	// names we got from schema
	targetEntity: string
	targetIndex: string
	targetFields: string[]
	// objects we found by those names
	refEntity!: Entity
	refIndex!: Field
	refFields!: Field[]

	constructor (f: IReferenceField, p?: SchemaNode) {
		super(f, p)
		if (f.targetEntity == null) {
			throw new Error(`${this.fullName()} targetEntity required.`)
		}
		if (typeof f.targetEntity !== 'string') {
			throw new Error(`${this.fullName()} targetEntity must be a string.`)
		}
		this.targetEntity = f.targetEntity

		if (f.targetIndex == null) {
			throw new Error(`${this.fullName()} targetIndex required.`)
		}
		if (typeof f.targetIndex !== 'string') {
			throw new Error(`${this.fullName()} targetIndex must be a string.`)
		}
		this.targetIndex = f.targetIndex

		if (f.targetFields == null) {
			throw new Error(`${this.fullName()} targetFields required.`)
		}
		if (!Array.isArray(f.targetFields)) {
			throw new Error(`${this.fullName()} targetFields must be an array of field names.`)
		}
		this.targetFields = f.targetFields

		if (f.format !== undefined && typeof f.format !== 'string') {
			throw new Error(`${this.fullName()} format property must be a string.`)
		}
		this.format = f.format

		if (f.promptText !== undefined && typeof f.promptText !== 'string') {
			throw new Error(`${this.fullName()} must be a string.`)
		}
		this.promptText = f.promptText
	}

	resolveReferences() {
		const s = this.getRoot() as Schema
		let ent: Entity | undefined
		let field: Field | undefined
		if (!(ent = s.getEntity(this.targetEntity))) {
			throw new Error(`Couldn't resolve reference to entity '${this.targetEntity}' for field ${this.fullName()}`)
		}
		this.refEntity = ent
		if (!(field = this.refEntity.getField(this.targetIndex))) {
			throw new Error(`Couldn't resolve reference to index field '${this.targetIndex}' for field ${this.fullName()}`)
		}
		this.refIndex = field
		this.refFields = []
		if (!this.targetFields || this.targetFields.length < 1) {
			throw new Error(`One or more targetFields required for field ${this.fullName()}`)
		}
		this.targetFields.forEach(fname => {
			const f = this.refEntity.getField(fname)
			if (!f) {
				throw new Error(`Couldn't resolve reference to targetField '${f}' for field ${this.fullName()}`)
			}
			this.refFields.push(f)
		})
	}
}
