import {SchemaNode} from '../schemanode'
import {IField, Field, Mode} from '../field'
import D from '../lib/dict'

const MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

export type DateTimeWidgetType = 'default' | 'pikaday'

export interface IDateTimeField extends IField {
	/** Which datetime input widget to use: 'default' (built-in) or 'pikaday' */
	widget?: DateTimeWidgetType
	/** Minimum date string */
	min?: string
	/** Maximum date string */
	max?: string
}

export class DateTimeField extends Field implements IDateTimeField {
	widget: DateTimeWidgetType
	min?: string
	max?: string
	minDate: Date
	maxDate: Date
	constructor (f: IDateTimeField, p?: SchemaNode) {
		super(f, p)
		this.widget = f.widget === 'pikaday' ? 'pikaday' : 'default'
		this.min = f.min
		this.max = f.max
		this.minDate = f.min ? new Date(f.min) : new Date('1900-01-01T00:00:00.000Z')
		this.maxDate = f.max ? new Date(f.max) : new Date('2099-12-31T23:59:59.000Z')
	}

	parseValue (str: string): any {
		return new Date(str)
	}

	validateData (data: D<any>, mode: Mode): string | void {
		const err = super.validateData(data, mode)
		if (err) return err
		const val = data[this.name]
		if (val == null || val === '') return
		if (typeof val !== 'string' && !val.getTime) {
			return `${this.name} is not a valid type of date.`
		}
		const d = new Date(val)
		if (Number.isNaN(d.getTime())) {
			return `${this.name} is not a valid date.`
		}
	}

	/**
	 * Format for display in local time.
	 */
	formatLocal (date: Date) {
		// TODO: Use moment (or something) for custom date formatting.
		if (!date || typeof date.toLocaleString !== 'function' ||
			Number.isNaN(date.getTime())
		) {
			return ''
		}
		let day = date.getDate().toString()
		if (day.length < 2) day = '0' + day
		const h = date.getHours()
		let h12 = (h === 12) ? '12' : (h % 12).toString()
		if (h12.length < 2) h12 = '0' + h12
		let m = date.getMinutes().toString()
		if (m.length < 2)
		m = '0' + m
		const ap = h < 12 ? 'AM' : 'PM'
		return MONTHS[date.getMonth()] +
			' ' + day +
			' ' + date.getFullYear() +
			' ' + h12 +
			':' + m +
			' ' + ap
	}

	formatStringLocal (dtStr: string) {
		return this.formatLocal(new Date(dtStr))
	}
}
