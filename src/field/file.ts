import {SchemaNode} from '../schemanode'
import {IField, Field} from '../field'

export interface IFileField extends IField {
	/** URL to upload to */
	uploadUrl: string
	/** Name to send along with upload. Defaults to fully-qualified field name. */
	uploadName?: string
	/** URL path to permanent storage directory (excluding filename) */
	filePath?: string
	/** URL path to temporary storage directory (excluding filename) */
	tempPath?: string
	/** Allowed extensions. (Eg: ['png','jpg']). Defaults to any. */
	extensions?: string[]
}

export class FileField extends Field implements IFileField {
	uploadUrl: string
	uploadName?: string
	filePath?: string
	tempPath?: string
	extensions?: string[]

	constructor(f: IFileField, p?: SchemaNode) {
		super(f, p)
		if (!f.uploadUrl || typeof f.uploadUrl !== 'string') {
			throw new Error(`url required for file field ${this.fullName()}, must be a string.`)
		}
		this.uploadUrl = f.uploadUrl
		if (f.uploadName != null) {
			if (typeof f.uploadName !== 'string') {
				throw new Error(`uploadName must be a string for file field ${this.fullName()}`)
			}
		} else {
			this.uploadName = this.fullName()
		}
		this.filePath = f.filePath
		this.tempPath = f.tempPath
		if (f.extensions && f.extensions.length > 0) {
			this.extensions = f.extensions.map(e => {
				return (e.charAt(0) === '.') ? e.substr(1) : e
			})
		}
	}
}
