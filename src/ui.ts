export interface NavLink {
	/** Text to display in UI */
	text: string
	/** URL to send request to. */
	url: string
	/** Request method. */
	method?: string
	/** Data to submit for non-GET requests. */
	data?: any
	/** Url to redirect to after request. */
	redirectUrl?: string
}

export interface NavSection {
	/** Optional section header text */
	text?: string
	/** If section should be collapsed by default. (Defaults to expaneded) */
	collapsed?: boolean
	/** All links in this section */
	links: NavLink[]
}

/**
 * Options for parts of the UI.
 */
export interface IUI {
	home?: {link: NavLink}
	topNav?: {links: NavLink[]}
	sideNav?: {sections: NavSection[]}
	welcome?: {title?: string, bodyText?: string, bodyHTML?: string}
}

export interface UI {
	home: {link: NavLink}
	topNav: {links: NavLink[]}
	sideNav: {sections: NavSection[]}
	welcome: {title?: string, bodyText?: string, bodyHTML?: string}
}

export function create (i: IUI = {}) {
	const ui = {
		home: i.home || {link: {text: "Home", url: "./"}},
		topNav: i.topNav || {links: []},
		sideNav: i.sideNav || {sections: []},
		welcome: i.welcome || {title: '', bodyText: '', bodyHTML: undefined}
	}
	return ui
}
