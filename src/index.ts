import {ISchemaNode, SchemaNode} from './schemanode'
import {IEntity, Entity} from './entity'
import {IUI, UI, create as createUI} from './ui'
import generateTSInterfaces from './modelgen/ts-interface'
import generateTypeORMClasses from './modelgen/ts-typeorm'

/**
 * Top-level schema. Represents a DB.
 */
export interface ISchema extends ISchemaNode {
	/** API endpoint for CRUD methods */
	dataApiUrl: string
	/** Optional hostname if not included in dataApiUrl */
	dataApiHost?: string
	/** List of entities */
	entities: IEntity[]
	/** UI options */
	ui?: IUI
	/** Path to static files used by admin client (not user uploaded files) */
	assetsRoot?: string
}

export class Schema extends SchemaNode implements ISchema {
	dataApiUrl: string
	dataApiHost?: string
	entities: Entity[]
	ui!: UI
	assetsRoot: string

	protected constructor (i: ISchema, p?: SchemaNode) {
		super(i, p)
		this.dataApiUrl = i.dataApiUrl
		this.dataApiHost = i.dataApiHost
		// If we got a separate host property we need to build the full URL
		if (i.dataApiHost) {
			this.dataApiUrl = '//' + i.dataApiHost + i.dataApiUrl
		}
		this.assetsRoot = i.assetsRoot
			? i.assetsRoot
			: './'
		if (!this.assetsRoot.endsWith('/')) {
			this.assetsRoot = this.assetsRoot + '/'
		}
		this.entities = i.entities.map(e => Entity.create(e, this))
	}

	/** Internal method - do not use */
	resolveReferences() {
		this.entities.forEach(e => {e.resolveReferences()})
	}

	static create (i: ISchema, p?: SchemaNode) {
		const schema = new Schema(i, p)
		schema.resolveReferences()
		schema.ui = createUI(i.ui)
		return schema
	}

	/**
	 * Find an entity in this schema by name. Returns undefined if not found.
	 */
	getEntity (ename: string) {
		return this.entities.find(e => e.name === ename)
	}

	/**
	 * Generate Typescript types for all entities
	 */
	generateTSTypes(
		options: {indentStyle?: '2space' | '4space' | 'tab'; semicolons?: boolean} = {}
	) {
		return generateTSInterfaces(this, options)
	}

	/**
	 * Generate TypeORM classes for all entities
	 */
	generateTypeORMClasses(
		options: {indentStyle?: '2space' | '4space' | 'tab'; semicolons?: boolean} = {}
	) {
		return generateTypeORMClasses(this, options)
	}
}
