import {capitalizeFirst} from '../lib/string'
import {ReferenceField} from '../field/reference'
import {ReferenceManyField} from '../field/reference-many'
import {Field} from '../field'
import {Entity} from '../entity'
import {Schema} from '..'
import tsTypeMap from './ts-typemap'

function columnAnnotations (field: Field) {
	const ent = field.parent as Entity
	if (field === ent.getPrimaryKey()) {
		return ['@PrimaryGeneratedColumn()']
	}
	if (field.type === 'reference') {
		const rf = field as ReferenceField
		return [`@OneToOne(type => ${capitalizeFirst(rf.refEntity.name)})`, '@JoinColumn()']
	}
	return ['@Column']
}

export default function generateTypeORMClasses(
	schema: Schema,
	{indentStyle = '2space', semicolons = true}: {indentStyle?: '2space' | '4space' | 'tab'; semicolons?: boolean} = {}
) {
	let src = ''
	schema.entities.forEach((ent, i) => {
		if (i > 0) src += '\n'
		src += '@Entity()\n'
		src += 'class ' + capitalizeFirst(ent.name) + ' {\n'
		ent.fields.forEach(field => {
			const indent = indentStyle === '2space' ? '  '
				: indentStyle === '4space' ? '    '
				: '\t'
			src += indent + columnAnnotations(field).join('\n' + indent) + '\n'
			src += indent + field.name + ': '
			if (field.type === 'reference') {
				src += capitalizeFirst((field as ReferenceField).refEntity.name) // tsTypeMap[(field as ReferenceField).refIndex.type]
			} else if (field.type === 'referenceMany') {
				src += tsTypeMap[(field as ReferenceManyField).refIndex.type] + '[]'
			} else {
				src += tsTypeMap[field.type]
			}
			if (field.nullable && !field.primaryKey && field.type !== 'referenceMany') {
				src += ' | null'
			}
			src += semicolons ? ';\n' : '\n'
		})
		src += '}\n'
	})
	return src
}
