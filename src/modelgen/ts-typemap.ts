const tsTypeMap: Record<string, string> = {
	boolean: 'boolean',
	int: 'number',
	long: 'number',
	float: 'number',
	double: 'number',
	order: 'number',
	string: 'string',
	email: 'string',
	url: 'string',
	password: 'string',
	phone: 'string',
	text: 'string',
	enum: 'string',
	enumInt: 'number',
	bits: 'number',
	datetime: 'Date',
	file: 'string',
	image: 'string'
}

export default tsTypeMap
