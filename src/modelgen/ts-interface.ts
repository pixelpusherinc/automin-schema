import {capitalizeFirst} from '../lib/string'
import {ReferenceField} from '../field/reference'
import {ReferenceManyField} from '../field/reference-many'
import {Schema} from '..'
import tsTypeMap from './ts-typemap'

export default function generateTSInterface(
	schema: Schema,
	{indentStyle = '2space', semicolons = true}: {indentStyle?: '2space' | '4space' | 'tab'; semicolons?: boolean} = {}
) {
	let src = ''
	schema.entities.forEach((ent, i) => {
		if (i > 0) src += '\n'
		src += 'interface ' + capitalizeFirst(ent.name) + ' {\n'
		ent.fields.forEach(field => {
			src += indentStyle === '2space' ? '  '
				: indentStyle === '4space' ? '    '
				: '\t'
			src += field.name + ': '
			if (field.type === 'reference') {
				src += tsTypeMap[(field as ReferenceField).refIndex.type]
			} else if (field.type === 'referenceMany') {
				src += tsTypeMap[(field as ReferenceManyField).refIndex.type] + '[]'
			} else {
				src += tsTypeMap[field.type]
			}
			if (field.nullable && !field.primaryKey && field.type !== 'referenceMany') {
				src += ' | null'
			}
			src += semicolons ? ';\n' : '\n'
		})
		src += '}\n'
	})
	return src
}
