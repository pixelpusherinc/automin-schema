//@ts-check

const fs = require('fs')
const {Schema} = require('../index')

/**
 * @param {string} outFormat
 * @param {string} schemaFilename
 */
async function run (outFormat, schemaFilename) {
	if (outFormat !== 'interface' && outFormat !== 'typeorm') {
		throw new Error("Unrecognized output format")
	}
	const json = JSON.parse(fs.readFileSync(schemaFilename, {encoding: 'utf8'}))
	const schema = Schema.create(json)
	const ts = outFormat === 'interface'
		? schema.generateTSTypes({indentStyle: 'tab', semicolons: false})
		: schema.generateTypeORMClasses({indentStyle: 'tab', semicolons: true})
	console.log(ts)
	process.exit(0)
}

const args = process.argv.slice(2)
if (args.length < 1) {
	console.log('Usage: genmodels <interface|typeorm> <schemafilename>')
	process.exit(1)
}

run(args[0], args[1])
