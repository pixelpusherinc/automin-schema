"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var schemanode_1 = require("./schemanode");
var entity_1 = require("./entity");
var ui_1 = require("./ui");
var ts_interface_1 = require("./modelgen/ts-interface");
var ts_typeorm_1 = require("./modelgen/ts-typeorm");
var Schema = /** @class */ (function (_super) {
    __extends(Schema, _super);
    function Schema(i, p) {
        var _this = _super.call(this, i, p) || this;
        _this.dataApiUrl = i.dataApiUrl;
        _this.dataApiHost = i.dataApiHost;
        // If we got a separate host property we need to build the full URL
        if (i.dataApiHost) {
            _this.dataApiUrl = '//' + i.dataApiHost + i.dataApiUrl;
        }
        _this.assetsRoot = i.assetsRoot
            ? i.assetsRoot
            : './';
        if (!_this.assetsRoot.endsWith('/')) {
            _this.assetsRoot = _this.assetsRoot + '/';
        }
        _this.entities = i.entities.map(function (e) { return entity_1.Entity.create(e, _this); });
        return _this;
    }
    /** Internal method - do not use */
    Schema.prototype.resolveReferences = function () {
        this.entities.forEach(function (e) { e.resolveReferences(); });
    };
    Schema.create = function (i, p) {
        var schema = new Schema(i, p);
        schema.resolveReferences();
        schema.ui = ui_1.create(i.ui);
        return schema;
    };
    /**
     * Find an entity in this schema by name. Returns undefined if not found.
     */
    Schema.prototype.getEntity = function (ename) {
        return this.entities.find(function (e) { return e.name === ename; });
    };
    /**
     * Generate Typescript types for all entities
     */
    Schema.prototype.generateTSTypes = function (options) {
        if (options === void 0) { options = {}; }
        return ts_interface_1.default(this, options);
    };
    /**
     * Generate TypeORM classes for all entities
     */
    Schema.prototype.generateTypeORMClasses = function (options) {
        if (options === void 0) { options = {}; }
        return ts_typeorm_1.default(this, options);
    };
    return Schema;
}(schemanode_1.SchemaNode));
exports.Schema = Schema;
