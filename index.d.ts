import { ISchemaNode, SchemaNode } from './schemanode';
import { IEntity, Entity } from './entity';
import { IUI, UI } from './ui';
/**
 * Top-level schema. Represents a DB.
 */
export interface ISchema extends ISchemaNode {
    /** API endpoint for CRUD methods */
    dataApiUrl: string;
    /** Optional hostname if not included in dataApiUrl */
    dataApiHost?: string;
    /** List of entities */
    entities: IEntity[];
    /** UI options */
    ui?: IUI;
    /** Path to static files used by admin client (not user uploaded files) */
    assetsRoot?: string;
}
export declare class Schema extends SchemaNode implements ISchema {
    dataApiUrl: string;
    dataApiHost?: string;
    entities: Entity[];
    ui: UI;
    assetsRoot: string;
    protected constructor(i: ISchema, p?: SchemaNode);
    /** Internal method - do not use */
    resolveReferences(): void;
    static create(i: ISchema, p?: SchemaNode): Schema;
    /**
     * Find an entity in this schema by name. Returns undefined if not found.
     */
    getEntity(ename: string): Entity | undefined;
    /**
     * Generate Typescript types for all entities
     */
    generateTSTypes(options?: {
        indentStyle?: '2space' | '4space' | 'tab';
        semicolons?: boolean;
    }): string;
    /**
     * Generate TypeORM classes for all entities
     */
    generateTypeORMClasses(options?: {
        indentStyle?: '2space' | '4space' | 'tab';
        semicolons?: boolean;
    }): string;
}
