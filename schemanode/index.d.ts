/**
 * Possible actions that can be performed on a schema object
 */
export declare type Action = 'show' | 'create' | 'edit' | 'delete';
/**
 * Base type for all schema nodes
 */
export interface ISchemaNode {
    /** Name for this node. Must be unique within its scope. */
    name: string;
    /** Human-friendly label for this node. Uses name if not supplied. */
    label?: string;
    /** Optional description text. */
    description?: string;
    /** Optional list of allowed actions. Defaults to all. */
    allowedActions?: Action[];
}
export declare abstract class SchemaNode implements ISchemaNode {
    name: string;
    label: string;
    description: string;
    allowedActions?: Action[];
    parent: SchemaNode | undefined;
    constructor(i: ISchemaNode, parent?: SchemaNode);
    /** Returns the root node of this schema tree */
    getRoot(): SchemaNode;
    /** For 2nd pass after schema parsed */
    resolveReferences(): void;
    fullName(): string;
    isAllowed(action: Action): boolean;
}
