"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SchemaNode = /** @class */ (function () {
    function SchemaNode(i, parent) {
        var _this = this;
        this.parent = parent || undefined;
        this.name = validateName(i.name, parent);
        this.label = i.label || this.name;
        this.description = i.description || '';
        if (i.allowedActions != null) {
            if (!Array.isArray(i.allowedActions)) {
                throw new Error("allowedActions for '" + this.fullName() + "' must be an array");
            }
            i.allowedActions.forEach(function (a) {
                if (a !== 'show' && a !== 'create' && a !== 'edit' && a !== 'delete') {
                    throw new Error("Unrecognized action in allowedActions for '" + _this.fullName() + "'");
                }
            });
            this.allowedActions = i.allowedActions;
        }
    }
    /** Returns the root node of this schema tree */
    SchemaNode.prototype.getRoot = function () {
        var p = this;
        while (!!p.parent)
            p = p.parent;
        return p;
    };
    /** For 2nd pass after schema parsed */
    SchemaNode.prototype.resolveReferences = function () { };
    SchemaNode.prototype.fullName = function () {
        return this.parent ? this.parent.name + '.' + this.name : this.name;
    };
    SchemaNode.prototype.isAllowed = function (action) {
        for (var p = this; p != null; p = p.parent) {
            if (p.allowedActions != null && p.allowedActions.indexOf(action) < 0) {
                return false;
            }
        }
        return true;
    };
    return SchemaNode;
}());
exports.SchemaNode = SchemaNode;
function validateName(name, parent) {
    var strParent = parent ? " for child of " + parent.name : "";
    if (!name) {
        throw new Error("A name is required" + strParent);
    }
    if (typeof name !== 'string') {
        throw new Error("name property must be a string" + strParent);
    }
    if (name.trim() !== name) {
        throw new Error("name property cannot start or end with spaces" + strParent);
    }
    return name;
}
