"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var string_1 = require("../lib/string");
var ts_typemap_1 = require("./ts-typemap");
function generateTSInterface(schema, _a) {
    var _b = _a === void 0 ? {} : _a, _c = _b.indentStyle, indentStyle = _c === void 0 ? '2space' : _c, _d = _b.semicolons, semicolons = _d === void 0 ? true : _d;
    var src = '';
    schema.entities.forEach(function (ent, i) {
        if (i > 0)
            src += '\n';
        src += 'interface ' + string_1.capitalizeFirst(ent.name) + ' {\n';
        ent.fields.forEach(function (field) {
            src += indentStyle === '2space' ? '  '
                : indentStyle === '4space' ? '    '
                    : '\t';
            src += field.name + ': ';
            if (field.type === 'reference') {
                src += ts_typemap_1.default[field.refIndex.type];
            }
            else if (field.type === 'referenceMany') {
                src += ts_typemap_1.default[field.refIndex.type] + '[]';
            }
            else {
                src += ts_typemap_1.default[field.type];
            }
            if (field.nullable && !field.primaryKey && field.type !== 'referenceMany') {
                src += ' | null';
            }
            src += semicolons ? ';\n' : '\n';
        });
        src += '}\n';
    });
    return src;
}
exports.default = generateTSInterface;
