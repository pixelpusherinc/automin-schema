import { Schema } from '..';
export default function generateTypeORMClasses(schema: Schema, { indentStyle, semicolons }?: {
    indentStyle?: '2space' | '4space' | 'tab';
    semicolons?: boolean;
}): string;
