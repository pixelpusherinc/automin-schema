"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var string_1 = require("../lib/string");
var ts_typemap_1 = require("./ts-typemap");
function columnAnnotations(field) {
    var ent = field.parent;
    if (field === ent.getPrimaryKey()) {
        return ['@PrimaryGeneratedColumn()'];
    }
    if (field.type === 'reference') {
        var rf = field;
        return ["@OneToOne(type => " + string_1.capitalizeFirst(rf.refEntity.name) + ")", '@JoinColumn()'];
    }
    return ['@Column'];
}
function generateTypeORMClasses(schema, _a) {
    var _b = _a === void 0 ? {} : _a, _c = _b.indentStyle, indentStyle = _c === void 0 ? '2space' : _c, _d = _b.semicolons, semicolons = _d === void 0 ? true : _d;
    var src = '';
    schema.entities.forEach(function (ent, i) {
        if (i > 0)
            src += '\n';
        src += '@Entity()\n';
        src += 'class ' + string_1.capitalizeFirst(ent.name) + ' {\n';
        ent.fields.forEach(function (field) {
            var indent = indentStyle === '2space' ? '  '
                : indentStyle === '4space' ? '    '
                    : '\t';
            src += indent + columnAnnotations(field).join('\n' + indent) + '\n';
            src += indent + field.name + ': ';
            if (field.type === 'reference') {
                src += string_1.capitalizeFirst(field.refEntity.name); // tsTypeMap[(field as ReferenceField).refIndex.type]
            }
            else if (field.type === 'referenceMany') {
                src += ts_typemap_1.default[field.refIndex.type] + '[]';
            }
            else {
                src += ts_typemap_1.default[field.type];
            }
            if (field.nullable && !field.primaryKey && field.type !== 'referenceMany') {
                src += ' | null';
            }
            src += semicolons ? ';\n' : '\n';
        });
        src += '}\n';
    });
    return src;
}
exports.default = generateTypeORMClasses;
